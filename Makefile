NAME = chick-8
VERSION = 0.0.3
AUTHOR = Evan Hanson <evhan@foldling.org>
URL = https://git.foldling.org/chick-8.git/

ENV = env VERSION=$(VERSION)
CSC_FLAGS = -O3 -d0 -X r7rs -X chick-8-syntax
CHICK_8_FLAGS = $(CSC_OPTIONS) $(CSC_FLAGS) -L -lncurses
CHICK_8_CURSES_FLAGS = $(CSC_OPTIONS) $(CSC_FLAGS) -R ncurses

DIR = $(NAME)-$(VERSION)
TGZ = $(DIR).tar.gz

CSC = csc
FPM = fpm
PANDOC = pandoc
CHICKEN_LINT = chicken-lint
CHICKEN_STATUS = chicken-status
CHICKEN_INSTALL = chicken-install

PREFIX = /usr
MANPREFIX = $(PREFIX)/share/man/man6
LIBPREFIX = $(PREFIX)/libexec
BINPREFIX = $(PREFIX)/bin

DEPS = begin-syntax matchable ncurses r7rs srfi-18

.PHONY: all clean deb deps lint bundle

all: chick-8

deps:
	@$(CHICKEN_INSTALL) $(DEPS) >/dev/null

chick-8: chick-8.scm chick-8-emulator.scm chick-8-curses.o
	@$(ENV) $(CSC) $(CHICK_8_FLAGS) -static chick-8.scm chick-8-curses.o -o $@

chick-8-curses.o: chick-8-curses.scm
	@$(ENV) $(CSC) $(CHICK_8_CURSES_FLAGS) -static chick-8-curses.scm -c -o $@

chick-8.1: chick-8.1.md
	@$(PANDOC) -s -f markdown -t man < $< > $@

bundle: chick-8 keymap
	@mkdir -p CHICK-8
	@cp -rp $^ CHICK-8
	@chmod 0755 CHICK-8
	@chmod 0755 CHICK-8/chick-8

dist: chick-8.1
	@mkdir -p $(DIR)
	@cp -rp Makefile LICENSE README.md keymap chick-8.1 *.scm $(DIR)
	@tar -czf $(DIR).tar.gz $(DIR)
	@$(RM) -r $(DIR)

lint:
	@$(ENV) $(CHICKEN_LINT) $(CSC_FLAGS) chick-8.scm
	@$(ENV) $(CHICKEN_LINT) $(CSC_FLAGS) chick-8-curses.scm

deb: bundle chick-8.1
	@install -m 0755 -d debian$(BINPREFIX)
	@install -m 0755 -d debian$(LIBPREFIX)
	@install -m 0755 -d debian$(MANPREFIX)
	@cp -rp CHICK-8 debian$(LIBPREFIX)
	@ln -fs $(LIBPREFIX)/CHICK-8/chick-8 debian$(BINPREFIX)
	@gzip -c chick-8.1 > chick-8.6.gz
	@install -m 0644 -t debian$(MANPREFIX) chick-8.6.gz
	@$(FPM) -f -s dir -t deb -C debian \
		--name $(NAME) \
		--version $(VERSION) \
		--maintainer "$(AUTHOR)" \
		--vendor "$(AUTHOR)" \
		--url $(URL) \
		--license BSD \
		--category games \
		--architecture native \
		--deb-changelog /dev/null \
		--description 'CHIP-8 interpreter' \
		--depends "libc6 (>= 2.3)" \
		--depends "libncurses5 (>= 5.0)"  \
		. >/dev/null

clean:
	@$(RM) -r chick-8 CHICK-8 debian *.o *.link *.deb *.gz
