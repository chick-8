;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; chick-8-emulator.scm - A CHIP-8 virtual machine.
;;;
;;; Copyright © 2016-2019 Evan Hanson. All Rights Reserved.
;;; BSD-style license. See LICENSE for details.
;;;
;;; This file is mostly R7RS and porting it to another Scheme shouldn't
;;; be too hard, but it does require srfi-18, srfi-60, and a handful of
;;; procedures defined to drive a user interface:
;;;
;;;   initialize-display
;;;   clear-display
;;;   draw-sprite
;;;   sound
;;;   key-pressed?
;;;   read-keypress
;;;

(import (scheme base)
        (chicken bitwise)
        (chicken random)
        (srfi 18))



;;;
;;; Execution logging.
;;;


(define debug-port
  (make-parameter #f))

(define (debug . args)
  (when (debug-port)
    (display (current-seconds) (debug-port))
    (display #\tab (debug-port))
    (for-each (lambda (x) (display x (debug-port))) args)
    (display #\newline (debug-port))
    (flush-output-port (debug-port))))

(define (base16 n)
  (number->string n 16))



;;
;; Font bits from http://devernay.free.fr/hacks/chip8/C8TECH10.HTM#font.
;;
(define fontset
  #u8(#xf0 #x90 #x90 #x90 #xf0   ; 0
      #x20 #x60 #x20 #x20 #x70   ; 1
      #xf0 #x10 #xf0 #x80 #xf0   ; 2
      #xf0 #x10 #xf0 #x10 #xf0   ; 3
      #x90 #x90 #xf0 #x10 #x10   ; 4
      #xf0 #x80 #xf0 #x10 #xf0   ; 5
      #xf0 #x80 #xf0 #x90 #xf0   ; 6
      #xf0 #x10 #x20 #x40 #x40   ; 7
      #xf0 #x90 #xf0 #x90 #xf0   ; 8
      #xf0 #x90 #xf0 #x10 #xf0   ; 9
      #xf0 #x90 #xf0 #x90 #x90   ; A
      #xe0 #x90 #xe0 #x90 #xe0   ; B
      #xf0 #x80 #x80 #x80 #xf0   ; C
      #xe0 #x90 #x90 #x90 #xe0   ; D
      #xf0 #x80 #xf0 #x80 #xf0   ; E
      #xf0 #x80 #xf0 #x80 #x80)) ; F



;;;
;;; Bytecode helpers.
;;;

(define-syntax <<
  (syntax-rules ()
    ((_ n m) (arithmetic-shift n m))))

(define-syntax >>
  (syntax-rules ()
    ((_ n m) (arithmetic-shift n (- m)))))

(define (bytevector-u16-ref v k)
  (let ((n1 (bytevector-u8-ref v (+ (* k 2) 0)))
        (n2 (bytevector-u8-ref v (+ (* k 2) 1))))
    (bitwise-ior (arithmetic-shift n1 8) n2)))

(define (bytevector-u16-set! v k x)
  (bytevector-u8-set! v (+ (* k 2) 0) (arithmetic-shift x -8))
  (bytevector-u8-set! v (+ (* k 2) 1) (bitwise-and x #xff)))



;;;
;;; Timers.
;;;

(define (current-seconds)
  (time->seconds (current-time)))

(define (make-timer n thunk)
  (lambda ()
    (let ((t (current-seconds)))
      (do ((d n (+ d n)))
          (#false)
        (thunk)
        (thread-sleep! (seconds->time (+ t d)))))))

(define (make-timer-register n box thunk)
  (make-timer
   n
   (lambda ()
     (when (positive? (car box))
       (thunk)
       (set-car! box (- (car box) 1))))))



;;;
;;; Emulator.
;;;

;;
;; Construct an emulator with the given CPU and hardware clock rates.
;;
;; At some point it would be nice to extend this with a handful of 0NNN
;; instructions to suspend execution, reset the VM, write to standard
;; output, and so on.
;;
(define (make-emulator process-rate timer-rate)

  (define M (make-bytevector 4096 0))
  (define S (make-bytevector 48 0))
  (define V (make-bytevector 16 0))

  (define d (list 0))
  (define s (list 0))

  (define delay-timer (make-timer-register timer-rate d void))
  (define sound-timer (make-timer-register timer-rate s sound))

  (lambda (program)
    (define start-time (current-seconds))
    (thread-start! delay-timer)
    (thread-start! sound-timer)
    (bytevector-copy! M #x0050 fontset)
    (bytevector-copy! M #x0200 program)
    (initialize-display process-rate timer-rate)
    (let step ((t 0.0)
               (I 0)
               (sp 0)
               (pc #x0200))
      (let ((op (bytevector-u16-ref M (/ pc 2)))
            (pc (+ pc 2)))
        (debug "M[" (base16 pc) "] = " (base16 op))
        (thread-sleep! (seconds->time (+ start-time t)))
        (case (bitwise-and op #xf000)
          ((#x0000)
           (case (bitwise-and op #x0fff)
             ((#x00e0)
              (clear-display))
             ((#x00ee)
              (set! sp (- sp 1))
              (set! pc (bytevector-u16-ref S sp))
              (bytevector-u16-set! S sp 0))
             (else
              (debug "invalid #x0000 instruction " (base16 op)))))
          ((#x1000)
           (set! pc (bitwise-and op #x0fff)))
          ((#x2000)
           (bytevector-u16-set! S sp pc)
           (set! pc (bitwise-and op #x0fff))
           (set! sp (+ sp 1)))
          ((#x3000)
           (let ((x (bitwise-and (>> op 8) #x000f))
                 (n (bitwise-and (>> op 0) #x00ff)))
             (when (= (bytevector-u8-ref V x) n)
               (set! pc (+ pc 2)))))
          ((#x4000)
           (let ((x (bitwise-and (>> op 8) #x000f))
                 (n (bitwise-and (>> op 0) #x00ff)))
             (unless (= (bytevector-u8-ref V x) n)
               (set! pc (+ pc 2)))))
          ((#x5000)
           (let ((x (bitwise-and (>> op 8) #x000f))
                 (y (bitwise-and (>> op 4) #x000f)))
             (when (= (bytevector-u8-ref V x) (bytevector-u8-ref V y))
               (set! pc (+ pc 2)))))
          ((#x6000)
           (let ((x (bitwise-and (>> op 8) #x000f))
                 (n (bitwise-and (>> op 0) #x00ff)))
             (bytevector-u8-set! V x n)))
          ((#x7000)
           (let ((x (bitwise-and (>> op 8) #x000f))
                 (n (bitwise-and (>> op 0) #x00ff)))
             (bytevector-u8-set!
              V x
              (bitwise-and (+ (bytevector-u8-ref V x) n) #xff))))
          ((#x8000)
           (let ((x (bitwise-and (>> op 8) #x000f))
                 (y (bitwise-and (>> op 4) #x000f)))
             (case (bitwise-and op #x000f)
               ((#x0000)
                (bytevector-u8-set! V x (bytevector-u8-ref V y)))
               ((#x0001)
                (bytevector-u8-set!
                 V x
                 (bitwise-ior (bytevector-u8-ref V x)
                              (bytevector-u8-ref V y))))
               ((#x0002)
                (bytevector-u8-set!
                 V x
                 (bitwise-and (bytevector-u8-ref V x)
                              (bytevector-u8-ref V y))))
               ((#x0003)
                (bytevector-u8-set!
                 V x
                 (bitwise-xor (bytevector-u8-ref V x)
                              (bytevector-u8-ref V y))))
               ((#x0004)
                (let* ((Vx  (bytevector-u8-ref V x))
                       (Vy  (bytevector-u8-ref V y))
                       (Vx’ (+ Vx Vy)))
                  (bytevector-u8-set! V x  (bitwise-and Vx’ #xff))
                  (bytevector-u8-set! V 15 (if (< Vx’ #x0f00) 0 1))))
               ((#x0005)
                (let* ((Vx  (bytevector-u8-ref V x))
                       (Vy  (bytevector-u8-ref V y))
                       (Vx’ (- Vx Vy)))
                  (bytevector-u8-set! V x  (bitwise-and Vx’ #xff))
                  (bytevector-u8-set! V 15 (if (< Vx’ #x0000) 0 1))))
               ((#x0006)
                (let* ((Vx  (bytevector-u8-ref V x))
                       (Vx’ (>> Vx 1)))
                  (bytevector-u8-set! V x  (bitwise-and Vx’ #xff))
                  (bytevector-u8-set! V 15 (bitwise-and Vx 1))))
               ((#x0007)
                (let* ((Vx  (bytevector-u8-ref V x))
                       (Vy  (bytevector-u8-ref V y))
                       (Vx’ (- Vy Vx)))
                  (bytevector-u8-set! V x  (bitwise-and Vx’ #xff))
                  (bytevector-u8-set! V 15 (if (< Vx’ #x0000) 0 1))))
               ((#x000e)
                (let* ((Vx  (bytevector-u8-ref V x))
                       (Vx’ (<< Vx 1)))
                  (bytevector-u8-set! V x  (bitwise-and Vx’ #xff))
                  (bytevector-u8-set! V 15 (>> (bitwise-and Vx 128) 7))))
               (else
                (debug "invalid #x8000 instruction " (base16 op))))))
          ((#x9000)
           (let ((x (bitwise-and (>> op 8) #x000f))
                 (y (bitwise-and (>> op 4) #x000f)))
             (unless (= (bytevector-u8-ref V x) (bytevector-u8-ref V y))
               (set! pc (+ pc 2)))))
          ((#xa000)
           (set! I (bitwise-and op #x0fff)))
          ((#xb000)
           (set! pc (+ (bytevector-u8-ref V 0) (bitwise-and op #x0fff))))
          ((#xc000)
           (let ((x (bitwise-and (>> op 8) #x000f))
                 (n (bitwise-and (>> op 0) #x00ff)))
             (bytevector-u8-set! V x (bitwise-and (pseudo-random-integer #x0100) n))))
          ((#xd000)
           (let ((x (bitwise-and (>> op 8) #x000f))
                 (y (bitwise-and (>> op 4) #x000f))
                 (n (bitwise-and (>> op 0) #x000f)))
             (case (draw-sprite (bytevector-u8-ref V x) (bytevector-u8-ref V y) n M I)
               ((#t) (bytevector-u8-set! V 15 1))
               (else (bytevector-u8-set! V 15 0)))))
          ((#xe000)
           (let ((x (bitwise-and (>> op 8) #x000f)))
             (case (bitwise-and op #x00ff)
               ((#x009e)
                (when (key-pressed? (bytevector-u8-ref V x))
                  (set! pc (+ pc 2))))
               ((#x00a1)
                (unless (key-pressed? (bytevector-u8-ref V x))
                  (set! pc (+ pc 2))))
               (else
                (debug "invalid #xe000 instruction " (base16 op))))))
          ((#xf000)
           (let ((x (bitwise-and (>> op 8) #x000f)))
             (case (bitwise-and op #x00ff)
               ((#x0007)
                (bytevector-u8-set! V x (car d)))
               ((#x000a)
                (bytevector-u8-set! V x (read-keypress))
                (set! t (- (current-seconds) start-time)))
               ((#x0015)
                (set-car! d (bytevector-u8-ref V x)))
               ((#x0018)
                (set-car! s (bytevector-u8-ref V x)))
               ((#x001e)
                (set! I (+ (bytevector-u8-ref V x) I)))
               ((#x0029)
                (set! I (+ (* (bytevector-u8-ref V x) 5) #x0050)))
               ((#x0033)
                (let ((Vx (bytevector-u8-ref V x)))
                  (bytevector-u8-set! M (+ I 0) (quotient Vx 100))
                  (bytevector-u8-set! M (+ I 1) (modulo (quotient Vx 10) 10))
                  (bytevector-u8-set! M (+ I 2) (modulo (modulo Vx 100) 10))))
               ((#x0055)
                (bytevector-copy! M I V 0 (+ x 1)))
               ((#x0065)
                (bytevector-copy! V 0 M I (+ x 1 I)))
               (else
                (debug "invalid #xf000 instruction " (base16 op))))))
          (else
           (debug "invalid instruction " (base16 op))))
        (when (thread-specific (current-thread))
          (thread-specific-set! (current-thread) #false)
          (set! start-time (- (current-seconds) t)))
        (unless (> pc 8192)
          (step (+ process-rate t) I sp pc))))))
