;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; chick-8.scm - A CHIP-8 interpreter.
;;;
;;; Copyright © 2016-2019 Evan Hanson. All Rights Reserved.
;;; BSD-style license. See LICENSE for details.
;;;

(declare
  (uses chick-8-curses)
  (no-argc-checks)
  (no-bound-checks))

(include "chick-8-emulator.scm")

(import (begin-syntax)
        (chicken condition)
        (chicken file)
        (chicken file posix)
        (chicken format)
        (chicken io)
        (chicken pathname)
        (chicken port)
        (chicken process signal)
        (chicken process-context)
        (chicken string)
        (matchable))



(begin-syntax
  (import (chicken platform))
  (or (feature? #:chicken-5)
      (syntax-error (conc "CHICKEN version 5 is required"))))

(define version
  (begin-syntax
    (import (chicken process-context))
    (or (get-environment-variable "VERSION")
        (syntax-error "No VERSION specified"))))

(define usage "usage: chick-8 [options ...] <file>
options:
  timer-speed:     n    (Hz)
  processor-speed: n    (Hz)
  keypress-weight: n    (cycles)
  keymap:          name (layout)")

(define (die reason . args)
  (when (condition? reason)
    (set! reason (error-object-message reason)))
  (fprintf (current-error-port) "~?~n" reason args)
  (exit 1))



(define (command-line-keyword-arguments)
  (map (lambda (s)
        (with-input-from-string s
          (lambda ()
            (let ((x (read)))
              (if (eof-object? (peek-char)) x s)))))
       (command-line-arguments)))

(define positive-integer?
  (conjoin integer? positive?))



(define (readable-file? x)
  (let ((f (->string x)))
    (and (file-exists? f)
         (file-readable? f)
         (regular-file? f))))

(define (keymap-path)
  (pathname-replace-file (executable-pathname) "keymap"))

(define (keymap x)
  (if (readable-file? x)
      (->string x)
      (make-pathname (keymap-path) (->string x))))

(define keymap-exists?
  (compose readable-file? keymap))

(define (read-keymap x)
  (handle-exceptions _
    (die "invalid keymap: ~a" x)
    (call-with-input-file
     (keymap x)
     (compose list->vector read-list))))

(define (read-program x)
  (call-with-input-file (sprintf "~a" x)
   (lambda (p)
     (begin0-let ((v (read-bytevector 65535 p)))
       (when (eof-object? v)
         (die "empty program: ~a" x))))))

(define (run file #!key (timer-speed 60) (processor-speed 600)
                        (keypress-weight #f) (keymap #f))
  (unless (not keymap)
    (set! *keymap* (read-keymap keymap)))
  (unless (not keypress-weight)
    (set! *keypress-weight* keypress-weight))
  ((make-emulator (/ 1 processor-speed) (/ 1 timer-speed))
   (read-program file)))

(define (main arguments)
  (let loop ((a arguments))
    (match a
      (('keymap: name . rest)
       (unless (keymap-exists? name) (die "invalid keymap: ~a" name))
       (loop rest))
      (((or 'timer-speed: 'processor-speed: 'keypress-weight:) n . rest)
       (unless (positive-integer? n) (die "invalid ~a ~a" (car a) n))
       (loop rest))
      (((or '-h '--help) . _)
       (print usage))
      (((or '-v '--version) . _)
       (print version))
      ((file)
       (unless (readable-file? file) (die "invalid program: ~a" file))
       (apply run file arguments))
      (else
       (die usage)))))

(define (install-signal-handlers! thread)
  (thread-specific-set! thread #false)
  (set-signal-handler! signal/int (lambda (_) (exit)))
  (set-signal-handler!
   signal/cont
   (lambda (_) (thread-specific-set! thread #true))))

(cond-expand
  ((or chicken-script compiling)
   (let ((args (command-line-keyword-arguments)))
     (install-signal-handlers! (current-thread))
     (handle-exceptions e (die e) (main args))))
  (else))
