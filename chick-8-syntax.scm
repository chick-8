;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; chick-8-syntax.scm - Helper macros and read-syntax.
;;;
;;; Copyright © 2016-2019 Evan Hanson. All Rights Reserved.
;;; BSD-style license. See LICENSE for details.
;;;

(import (chicken bitwise)
        (chicken condition)
        (ncurses))

(define-syntax begin0-let
  (syntax-rules ()
    ((_ (r ... (a e)) . b)
     (let (r ... (a e)) (begin . b) a))))

(define-syntax catch
  (syntax-rules ()
    ((_ x) (catch () x))
    ((_ c x) (condition-case x (e c e)))))

(char-name 'none (integer->char ERR))
(char-name 'resize (integer->char KEY_RESIZE))
(char-name 'block (integer->char (bitwise-ior 32 A_REVERSE)))
