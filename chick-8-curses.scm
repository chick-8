;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; chick-8-curses.scm - An ncurses interface for CHICK-8.
;;;
;;; Copyright © 2016-2019 Evan Hanson. All Rights Reserved.
;;; BSD-style license. See LICENSE for details.
;;;

(declare
  (unit chick-8-curses)
  (fixnum-arithmetic)
  (no-argc-checks)
  (no-bound-checks)
  (export *keymap* *keypress-weight*)
  (export initialize-display clear-display)
  (export key-pressed? read-keypress)
  (export draw-sprite sound))

(import (chicken bitwise)
        (chicken condition)
        (srfi 4)
        (srfi 18))



;;;
;;; Exported globals.
;;;

;; Sets the approximate number of clock ticks a keypress should span.
(define *keypress-weight* 80)

;;
;; Key translations for those without a dedicated CHIP-8 keypad.
;;
;; An edge case, I know.
;;
(define *keymap*
  #(#\x #\1 #\2 #\3   ; 0 1 2 3
    #\q #\w #\e #\a   ; 4 5 6 7
    #\s #\d #\z #\c   ; 8 9 A B
    #\4 #\r #\f #\v)) ; C D E F



;;;
;;; Utilities.
;;;

;; Find element `x` in vector `v` using comparator `f`.
(define (vector-index v x f)
  (let ((l (vector-length v)))
    (let loop ((i 0))
      (cond ((= i l) #f)
            ((f x (vector-ref v i)) i)
            (else (loop (+ i 1)))))))

;; Which bits are set in `v1` but not in `v2`?
(define (bitmask-off v1 v2)
  (bitwise-and (bitwise-xor v2 v1) v1))

;; Trigger a thunk on condition variable specific `c`.
(define ((make-trigger m c thunk))
  (do () (#f)
    (mutex-lock! m)
    (cond ((condition-variable-specific c)
           (condition-variable-specific-set! c #f)
           (thunk)
           (mutex-unlock! m))
          (else
           (mutex-unlock! m c)))))



;;;
;;; Rudimentary, inefficient ncurses graphics.
;;;

(define G (make-u8vector 256 0))

(define *window* #f)
(define *window-width* 66)  ; 64 + 2 border
(define *window-height* 34) ; 32 + 2 "

(define-values (screen redraw)
  (values (make-mutex) (make-condition-variable)))

(define (request-redraw)
  (condition-variable-specific-set! redraw #t)
  (condition-variable-signal! redraw))

(define (create-window)
  (let ((w (newwin *window-height* *window-width* 0 0)))
    (wtimeout w 0)
    (set! *window* w)))

(define (resize-handler _)
  (endwin)
  (request-redraw))

(define (draw-char x y c)
  (catch (curses) (mvwaddch *window* (+ y 1) (+ x 1) c)))

(define (clear-display)
  (do ((i 0 (+ i 1)))
      ((= i 256) (request-redraw))
    (u8vector-set! G i 0)))

(define (coordinates->index x y)
  (let ((i (+ (quotient x 8) (* y 8))))
    (and (< i 256) i)))

(define (draw x y z n M i)
  (let loop ((n n) (i i) (y y) (c 0))
    (let ((i’ (coordinates->index x y)))
      (if (or (zero? n) (not i’))
          (positive? c)
          (let* ((m  (u8vector-ref M i))
                 (g  (u8vector-ref G i’))
                 (g’ (bitwise-xor (arithmetic-shift m z) g)))
            (u8vector-set! G i’ (bitwise-and g’ #xff))
            (loop (- n 1) (+ i 1) (+ y 1)
                  (bitwise-ior (bitmask-off g g’) c)))))))

(define (draw-sprite x y n M i)
  (let* ((z  (remainder x 8))
         (d1 (draw x y (- z) n M i))
         (d2 (and (positive? z)
                  (draw (+ x 8) y (- 8 z) n M i))))
    (request-redraw)
    (or d1 d2)))

(define (update-display)
  (catch (curses) (wresize *window* *window-height* *window-width*))
  (catch (curses) (wborder *window* 0 0 0 0 0 0 0 0))
  (do ((y 0 (+ y 1)))
      ((= y 32) (wrefresh *window*))
    (do ((x 0 (+ x 8)))
        ((= x 64))
      (let ((n (u8vector-ref G (coordinates->index x y))))
        (do ((i 7 (- i 1))
             (x x (+ x 1)))
            ((< i 0))
          (draw-char x y (if (bit->boolean n i) #\block #\space)))))))



;;;
;;; Keypress handling.
;;;

(define K (make-vector 16 0))

(define (key-pressed? n)
  (positive? (vector-ref K n)))

(define (read-event)
  (begin0-let ((e (wgetch *window*)))
    (when (char=? e #\resize) (resize-handler e))))

(define (age-keypresses)
  (do ((i 0 (+ i 1)))
      ((= i 16))
    (let ((n (vector-ref K i)))
      (when (< 0 n)
        (vector-set! K i (- n 1))))))

(define (handle-event e)
  (and-let* ((i (vector-index *keymap* e eq?)))
    (vector-set! K i *keypress-weight*)))

(define (handle-events)
  (do ((e #\null (read-event)))
      ((char=? e #\none) (age-keypresses))
    (handle-event e)))

(define (read-keypress)
  (wtimeout *window* -1)
  (let ((e (read-event)))
    (wtimeout *window* 0)
    (or (vector-index *keymap* e eq?)
        (begin (thread-yield!)
               (read-keypress)))))



;;;
;;; Other stuff.
;;;

;; For sound we just use the terminal bell.
(define (sound) (beep))

;; "Hardware" initialization, to be called at boot.
(define (initialize-display process-rate timer-rate)
  (initscr)
  (cbreak)
  (noecho)
  (curs_set 0)
  (create-window)
  (on-exit endwin)
  (thread-start! (make-timer process-rate handle-events))
  (thread-start! (make-trigger screen redraw update-display))
  (current-exception-handler
   (let ((handler (current-exception-handler)))
     (lambda (e)
       (endwin)
       (handler e)))))
