CHICK-8
=======

CHICK-8 is an ncurses-based CHIP-8 emulator.

Dependencies
------------

 * [make](https://www.gnu.org/software/make/make.html)
 * [ncurses](https://www.gnu.org/software/ncurses/ncurses.html)
 * [CHICKEN Scheme](https://call-cc.org/)
    * [begin-syntax](https://wiki.call-cc.org/egg/begin-syntax)
    * [matchable](https://wiki.call-cc.org/egg/matchable)
    * [ncurses](https://wiki.call-cc.org/egg/ncurses)
    * [r7rs](https://wiki.call-cc.org/egg/r7rs)
    * [srfi-18](https://wiki.call-cc.org/egg/srfi-18)

Building
--------

To install dependencies and build `chick-8`:

    $ make deps
    $ make

To build a self-contained, relocatable application directory:

    $ make bundle

Usage
-----

    usage: chick-8 [options ...] <file>
    options:
      timer-speed:     n    (Hz)
      processor-speed: n    (Hz)
      keypress-weight: n    (cycles)
      keymap:          name (layout)

The program's default parameters are:

    $ chick-8 timer-speed: 60 processor-speed: 600 keypress-weight: 80

More information about CHICK-8 and how to use it can be found in the
program's [manual page][man].

[man]: https://git.foldling.org/chick-8.git/tree/master/chick-8.1.md

CHIP-8 programs of diverse quality can be found scattered across the
web, for example [here][1] and [here][2]. CHICK-8 should run
any valid CHIP-8 ROM, though you may have to adjust its processor speed
slightly for some games in order to avoid missing sprites when the
game's draw cycle aligns poorly with the terminal redraw rate.

[1]: http://devernay.free.fr/hacks/chip8/
[2]: http://www.zophar.net/pdroms/chip8.html

License
-------

Copyright (c) 2016-2019 Evan Hanson <evhan@foldling.org>

BSD-style license. See LICENSE for details.
