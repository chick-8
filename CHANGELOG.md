0.0.3
==========
- Ported to CHICKEN 5.
- Added “--help” and “--version” options.
- A French (AZERTY) keymap has been added.

0.0.2
=====
- Users can now specify an alternative keymap with the “keymap:” option.
  A built-in keymap for German keyboards is included.
- Added support for suspending and resuming the emulator with
  SIGSTOP/SIGTSTP (i.e. Control-Z) and SIGCONT (i.e. `fg`).

0.0.1
=====
- Initial release.
