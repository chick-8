% CHICK-8(6) Version 0.0.3 | CHICK-8 User Manual
% Evan Hanson <evhan@foldling.org>
% Feb 2019

# NAME

**chick-8** - CHIP-8 emulator.

# SYNOPSIS

**chick-8** \[*option* ...] *file*

# DESCRIPTION

**chick-8** is an ncurses-based emulator for CHIP-8 bytecode.

*file* should be a CHIP-8 ROM. The emulator's video is displayed within
a fixed-size 64-by-32 ncurses window, while sound is represented by the
terminal bell. For input, the following keys are mapped to the 16
buttons of the CHIP-8 keypad by default:

    +---------------+
    | 1 | 2 | 3 | 4 |
    |---|---|---|---|
    | q | w | e | r |
    |---|---|---|---|
    | a | s | d | f |
    |---|---|---|---|
    | z | x | c | v |
    +---------------+

An alternative keymap can be specified with the **keymap:** option.

The emulator can be terminated at any time with an interrupt signal
(Control-C).

# OPTIONS

processor-speed: **\<number> (Hz)**
:   Adjusts the CPU speed of the emulator. The default value is **600
Hz**.

timer-speed: **\<number> (Hz)**
:   Adjusts the speed of the emulator's **delay** and **sound** timers.
The default value is **60 Hz**, which is the speed generally assumed by
CHIP-8 programs.

keypress-weight: **\<number> (cycles)**
:   Adjusts the number of clock cycles spanned by a single keypress.
Increasing this value causes keypresses to be “heavier”, or
longer-lasting with respect to the interpreted program's progress. The
default value is **80 cycles**.

keymap: **\<name> (layout)**
:   Specifies the keyboard mapping to use for the emulator's keypad.
**\<name>** may specify a built-in keymap or a custom keymap file. Refer
to “KEYMAPS” for more information.

# KEYMAPS

The following predefined keymaps are supported:

 - “hex” maps the 0-9 and a-f keys to their respective hex values.
 - “de” is suitable for QWERTZ keyboards.
 - “fr” is suitable for AZERTY keyboards.
 - “us” is suitable for QWERTY keyboards. This is the default keymap.

# CAVEATS

For some games it may be necessary to adjust the emulator's processor
speed slightly in order to avoid missing sprites when the game's draw
cycle aligns poorly with the terminal redraw rate.

# SEE ALSO

`ncurses(3)`
